# Crud & adminLTE integration example with Laravel 5.8

This project was made with Laravel 5.8 (PHP 7.4.12)

# Requirements

Laravel (version 5.8 recommanded).

wamp or xamp or mamp...

# Steps for Project download and setup with database config

1. Download the project from gitlab as a zip.
2. Unzip the project in the Laravel folder, after unziping you will get a folder named "queo_test-master"
3. Open the folder and search for a file named "laracrudforqueo.sql" which is mySql database file (sql file)
4. Start your local server (wamp, xamp or mamp or..) start mysql also, go to Localhost->phpMyAdmin (choose mySql) and click on "IMPORT" and choose the sql file "laracrudforqueo.sql", choose utf-8 and click execute.
5. Now the database is added, but if your identifiers for the phpMyAdmin are different than "user : 'root', password : '' " you need to follow the instructions below:

+ Go to the unziped folder (project folder) and search for a file named ".env" open it with your text/code editor.
+ once opened in the text editor, change this lines, "DB_USERNAME=root", replace 'root' with your phpMyAdmin username, "DB_PASSWORD=" , add your password after "=".
+ Save and quit. The database is now sync with the project.

6. Now you need to install adminLTE in your project, for that we will use "composer" if you have it go to the next step (7) if not, you need to install it, for that download the installer from this link [composer-setup.exe](https://getcomposer.org/Composer-Setup.exe)  and install it.
7. Open your command prompt (CMD) and type the commands below :

+ cd C:\wamp64\www\Laravel\queo_test-master  .  Replace "C:\wamp64\www\Laravel\queo_test-master" with the path to your unziped project folder.
+ composer require "almasaeed2010/adminlte=~2.4"    .  This command will install adminLTE in your project folder. DO NOT CLOSE THE CMD WINDOW AFTER INSTALLATION FINISH.

8. Now we will lunch the server, type this commands in the same CMD window :

+ php artisan serve    .  This command will lunch the server, as an answer it will generate an IP address for the server, copy it and paste it in a browser link bar and hit enter.

9. Normally you will see the Laravel main page with Register/Login options, that means the project has been successfuly loaded.
10. Register an account and login with it, or use this existing already created account, infos : email : "queo@admin.com" , password : "queoadmin".
11. Once logged in, you the "Home" page will load which has adminLTE template, all of the components were deleted, only few of them were left, The page has 4 sections: 

+ A nav-bar (Header) with some components on the right from adminLTE, and "Home" which leads to the Home page, "Cities" which leads to the crud page. 
+ Side bar (Personnal or admin bar) with "Queo Test" in the top which leads to the "Home" page, The admin username is displayed beside a standard image from adminLTE, and some components left just for example (categories/news) not working, but the "Logout" button works to logout.
+ The main section for content, it is empty with a simple text "Queo Test".
+ A footer.

12. The crud is accessible from "Cities" component from the nav-bar (header), it is a simple cities list with option to add/update/delete as requested.
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/testhome', 'HomeController@test')->name('testhome');

Route::middleware(['auth'])->prefix('admin')->name('admin.')->group(function() {
    Route::resource('cities','Admin\CityController');

});

Route::namespace('Admin')->prefix('admin')->as('admin.')->middleware('auth')->group(function(){
    Route::get('/', 'HomeController@index')->name('home');
	Route::resource('/categories', 'CategoriesController');
    Route::resource('/news', 'NewsController');
    
});

